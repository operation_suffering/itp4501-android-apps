package android.exercise.matchpairsmemorygame;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class YourRecordsActivity extends AppCompatActivity {
    SQLiteDatabase db;
    ListView listView;
    String[] column = {"sPlayerName", "sMovesCount", "sPlayTime", "sDate"};
    List<String> recordsList = new ArrayList<>(); // Initialize the recordsList variable
    ArrayAdapter<String> adapter;
    Cursor cursor = null;
    private MediaPlayerApplication mediaPlayerApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.your_records_activity);

        mediaPlayerApplication = (MediaPlayerApplication) getApplicationContext();

        listView = findViewById(R.id.listView);

        // Create an instance of the SQLiteOpenHelper
        MyDatabaseHelper dbHelper = new MyDatabaseHelper(this);

        // Get a writable database
        db = dbHelper.getWritableDatabase();

        // Set item click listener for the ListView
        listView.setOnItemClickListener((parent, view, position, id) -> {
            // Handle item click if needed
            String selectedItem = recordsList.get(position);
            Toast.makeText(YourRecordsActivity.this, "Clicked: " + selectedItem, Toast.LENGTH_SHORT).show();
        });

        // Initialize the records list and adapter
        recordsList = new ArrayList<>();
        adapter = new ArrayAdapter<>(this, R.layout.listview, recordsList);
        listView.setAdapter(adapter);

        // Show the records immediately when the activity starts
        showRecords();

        // Find the "Clear Records" button and add a click listener
        Button clearButton = findViewById(R.id.clearButton);
        clearButton.setOnClickListener(v -> clearRecords());
    }


    public void showRecords() {
        try {
            cursor = db.query("Player", column, null, null, null, null, "sDate DESC");
            if (cursor != null && cursor.moveToFirst()) {
                recordsList.clear(); // Clear the existing records
                do {
                    @SuppressLint("Range") String playerName = cursor.getString(cursor.getColumnIndex("sPlayerName"));
                    @SuppressLint("Range") int movesCount = cursor.getInt(cursor.getColumnIndex("sMovesCount"));
                    @SuppressLint("Range") int playTime = cursor.getInt(cursor.getColumnIndex("sPlayTime"));
                    @SuppressLint("Range") String date = cursor.getString(cursor.getColumnIndex("sDate"));

                    @SuppressLint("DefaultLocale") String record = String.format("Player: %s\nDate: %s\nMoves: %d\nTime: %d sec",
                            playerName, date, movesCount, playTime);
                    recordsList.add(record); // Add the record to the list
                } while (cursor.moveToNext());
            } else {
                Toast.makeText(this, "No records found.", Toast.LENGTH_SHORT).show();
            }
        } catch (SQLException e) {
            Toast.makeText(this, "Error retrieving records: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        adapter.notifyDataSetChanged(); // Notify the adapter about the data changes
    }

    // SQLiteOpenHelper subclass for managing database creation and versioning
    private static class MyDatabaseHelper extends SQLiteOpenHelper {
        private static final String DATABASE_NAME = "YourRecords.db";
        private static final int DATABASE_VERSION = 1;

        private static final String CREATE_RECORDS_TABLE =
                "CREATE TABLE IF NOT EXISTS Player (sPlayerName TEXT, sMovesCount INTEGER, sPlayTime INTEGER, sDate TEXT)";

        public MyDatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_RECORDS_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // Drop the table if it exists and create a new one
            db.execSQL("DROP TABLE IF EXISTS Player");
            onCreate(db);
        }

    }

    public void clearRecords() {
        try {
            db.delete("Player", null, null); // Delete all records from the Player table

            // Clear the records list
            recordsList.clear();

            // Fetch the updated records from the database
            showRecords();

            Toast.makeText(this, "Records cleared successfully.", Toast.LENGTH_SHORT).show();
        } catch (SQLException e) {
            Toast.makeText(this, "Error clearing records: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mediaPlayerApplication.pauseBackgroundMusic();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mediaPlayerApplication.resumeBackgroundMusic();
    }

}
