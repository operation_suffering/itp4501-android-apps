package android.exercise.matchpairsmemorygame;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyDatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "YourRecords.db";
    private static final int DATABASE_VERSION = 1;

    private static final String CREATE_RECORDS_TABLE =
            "CREATE TABLE Player (sPlayerName TEXT, sMovesCount INTEGER, sPlayTime TEXT, sDate TEXT)";

    public MyDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_RECORDS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop the table if it exists and create a new one
        db.execSQL("DROP TABLE IF EXISTS Player");
        onCreate(db);
    }
}
