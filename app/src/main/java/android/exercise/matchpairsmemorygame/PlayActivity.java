package android.exercise.matchpairsmemorygame;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class PlayActivity extends AppCompatActivity {

    private GridLayout gameBoard;
    private List<Integer> buttonValues;
    private List<ImageButton> clickedButtons;
    private List<Integer> clickedButtonValues;
    int movesCount = 0;
    private long startTime; // to store the start time
    private boolean timerRunning = false; // to track if the timer is currently running
    private final Handler timerHandler = new Handler(); // handler to update the timer
    private MediaPlayerApplication mediaPlayerApplication;


    @SuppressLint("SetTextI18n")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_activity);

        gameBoard = findViewById(R.id.gameBoard);
        buttonValues = new ArrayList<>();
        clickedButtons = new ArrayList<>();
        clickedButtonValues = new ArrayList<>();
        mediaPlayerApplication = (MediaPlayerApplication) getApplicationContext();


        // Add the button values
        buttonValues.add(1);
        buttonValues.add(1);
        buttonValues.add(2);
        buttonValues.add(2);
        buttonValues.add(3);
        buttonValues.add(3);
        buttonValues.add(4);
        buttonValues.add(4);

        // Shuffle the button values
        Collections.shuffle(buttonValues);

        // Add click listeners to the buttons
        for (int i = 0; i < gameBoard.getChildCount(); i++) {
            View child = gameBoard.getChildAt(i);
            if (child instanceof ImageButton) {
                final ImageButton button = (ImageButton) child;
                final int index = i;
                button.setOnClickListener(v -> {
                    // Handle button click
                    int value = buttonValues.get(index);
                    button.setImageResource(getImageResource(value));
                    button.setEnabled(false);
                    processClickedButton(button, value);
                    mediaPlayerApplication.playButtonSound();

                });

                // Set the image resource to transparent
                button.setImageResource(android.R.color.transparent);
            }
        }
    }

    private int getImageResource(int value) {
        switch (value) {
            case 1:
                return R.drawable.cat_1;
            case 2:
                return R.drawable.cat_2;
            case 3:
                return R.drawable.cat_3;
            case 4:
                return R.drawable.cat_4;
            default:
                return 0;
        }
    }

    // Process click button, when 2 button clicked, delay for 1 second and then check if they are the same.
    // If false, set the buttons to transparent and enable them again.
    // If true, set the buttons to invisible and clear the lists.
    private void processClickedButton(ImageButton button, int value) {
        clickedButtons.add(button);
        clickedButtonValues.add(value);

        if (!timerRunning) {
            startTimer();
            timerRunning = true;
        }

        if (clickedButtons.size() == 2) {
            disableAllButtons(); // Disable all buttons during the delay

            // Delay for 1 second before checking the button values
            new Handler().postDelayed(() -> {
                if (clickedButtonValues.get(0).equals(clickedButtonValues.get(1))) {
                    // If buttons are the same, set them to invisible and clear the lists
                    clickedButtons.get(0).setVisibility(View.INVISIBLE);
                    clickedButtons.get(1).setVisibility(View.INVISIBLE);
                    mediaPlayerApplication.playMatchSound();
                } else {
                    // If buttons are not the same, set them back to transparent
                    for (ImageButton clickedButton : clickedButtons) {
                        clickedButton.setImageResource(android.R.color.transparent);
                        clickedButton.setEnabled(true);
                    }
                }

                clickedButtons.clear();
                clickedButtonValues.clear();
                checkAllButtonsInvisible();
                countMoves(); // Call countMoves() method here
                enableAllButtons(); // Enable all buttons after the delay
            }, 1000);
        }
    }


    private void disableAllButtons() {
        for (int i = 0; i < gameBoard.getChildCount(); i++) {
            View child = gameBoard.getChildAt(i);
            if (child instanceof ImageButton) {
                ImageButton button = (ImageButton) child;
                button.setEnabled(false);
            }
        }
    }

    private void enableAllButtons() {
        for (int i = 0; i < gameBoard.getChildCount(); i++) {
            View child = gameBoard.getChildAt(i);
            if (child instanceof ImageButton) {
                ImageButton button = (ImageButton) child;
                button.setEnabled(true);
            }
        }
    }


    // Check if all clickedButtons are clear, if true, make the continueButton visible
    public void checkAllButtonsInvisible() {
        boolean allButtonsInvisible = true;
        for (int i = 0; i < gameBoard.getChildCount(); i++) {
            View child = gameBoard.getChildAt(i);
            if (child instanceof ImageButton) {
                final ImageButton button = (ImageButton) child;
                if (button.getVisibility() == View.VISIBLE) {
                    allButtonsInvisible = false;
                    break;
                }
            }
        }
        if (allButtonsInvisible) {
            stopTimer(); // Stop the timer when all buttons are invisible

            // Play the sound of finish
            mediaPlayerApplication.playFinishSound();
            showSaveRecordDialog(); // Prompt the player to save the record

            Button continueButton = findViewById(R.id.continueButton);
            continueButton.setVisibility(View.VISIBLE);
            continueButton.setOnClickListener(v -> {
                rerunPlayActivity();// Prompt the player to replay
            });
        }
    }

    // Count the number of moves when 2 buttons are clicked
    @SuppressLint("SetTextI18n")
    public void countMoves() {
        TextView moves = findViewById(R.id.movesTextView);
        movesCount++;
        moves.setText("Moves: " + movesCount);
    }

    private void startTimer() {
        startTime = System.currentTimeMillis(); // record the start time

        // post a runnable to the handler to update the timer every second
        timerHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                updatePlayTime();
                timerHandler.postDelayed(this, 1000); // schedule the next update after 1 second
            }
        }, 1000);
    }

    @SuppressLint("SetTextI18n")
    private void updatePlayTime() {
        long currentTime = System.currentTimeMillis();
        long elapsedTime = currentTime - startTime;
        int seconds = (int) (elapsedTime / 1000);

        TextView playTime = findViewById(R.id.playTimeTextView);
        playTime.setText("Play time: " + seconds + "s");
    }

    private void stopTimer() {
        timerHandler.removeCallbacksAndMessages(null); // remove all callbacks and messages from the handler
        timerRunning = false;
    }

    //show dialog to ask if player wants to save record.
    // If yes, prompt player to enter name and save record.
    // If no, go back to main activity.
    private void showSaveRecordDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Congratulations! You finish in " + movesCount + " moves! and " + getPlayTime() + " seconds!");
        builder.setMessage("Do you want to save your record? \n Type your name below:");

        // Add an input field for the player's name
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton("Yes", (dialog, which) -> {
            String playerName = input.getText().toString().trim();
            if (!playerName.isEmpty()) {
                // TODO: Save the record with the player's name
                // You can pass the playerName to the saveRecord() method or any other logic you have for saving the record.
                saveRecord(playerName, movesCount, getPlayTime());
            }
        });

        builder.setNegativeButton("No", (dialog, which) -> dialog.dismiss());

        builder.show();
    }

    private void rerunPlayActivity() {
        Intent intent = getIntent();
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private String getPlayTime() {
        long currentTime = System.currentTimeMillis();
        long elapsedTime = currentTime - startTime;
        int seconds = (int) (elapsedTime / 1000);
        return seconds + "sec";
    }

    //To save the record to the database
    //The data will be show in play_activity.xml and PlayActivity.java with ListView.
    //The latest 20 records will be shown in the ListView.
    public void saveRecord(String playerName, int movesCount, String playTime) {
        try {
            MyDatabaseHelper dbHelper = new MyDatabaseHelper(this);
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put("sDate", getCurrentDateTime()); // Save the current date and time
            values.put("sPlayerName", playerName);
            values.put("sMovesCount", movesCount);
            values.put("sPlayTime", playTime);

            long newRowId = db.insert("Player", null, values);
            if (newRowId != -1) {
                Toast.makeText(this, "Record saved successfully", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Failed to save record", Toast.LENGTH_SHORT).show();
            }

            db.close();
        } catch (Exception e) {
            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
        }
    }

    private String getCurrentDateTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date currentDate = new Date();
        return sdf.format(currentDate);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mediaPlayerApplication.pauseBackgroundMusic();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mediaPlayerApplication.resumeBackgroundMusic();
    }
}
