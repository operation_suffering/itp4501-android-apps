package android.exercise.matchpairsmemorygame;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;


public class MainActivity extends AppCompatActivity {

    private MediaPlayerApplication mediaPlayerApplication;
    private boolean isSoundMuted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mediaPlayerApplication = (MediaPlayerApplication) getApplicationContext();


        Button playButton = findViewById(R.id.playButton);
        playButton.setOnClickListener(v -> {
            // Start the Play activity
            Intent intent = new Intent(MainActivity.this, PlayActivity.class);
            startActivity(intent);
        });

        Button gameRankingButton = findViewById(R.id.gameRankingButton);
        gameRankingButton.setOnClickListener(v -> {
            // Start the GameRanking activity
            Intent intent = new Intent(MainActivity.this, GameRankingActivity.class);
            startActivity(intent);
        });

        Button yourRecordsButton = findViewById(R.id.yourRecordsButton);
        yourRecordsButton.setOnClickListener(v -> {
            // Start the YourRecords activity
            Intent intent = new Intent(MainActivity.this, YourRecordsActivity.class);
            startActivity(intent);
        });

        Button closeButton = findViewById(R.id.closeButton);
        closeButton.setOnClickListener(v -> {
            // Stop the background music
            mediaPlayerApplication.stopBackgroundMusic();
            // Close the app entirely
            finishAffinity();
            System.exit(0); // Optional: Forcefully terminate the app process
        });


        ImageButton catImageButton = findViewById(R.id.catImageButton);
        catImageButton.setOnClickListener(v -> {
            int[] catImages = {
                    R.drawable.cat_1,
                    R.drawable.cat_2,
                    R.drawable.cat_3,
                    R.drawable.cat_4
            };
            int randomIndex = new Random().nextInt(catImages.length);
            int randomCatImage = catImages[randomIndex];
            catImageButton.setImageResource(randomCatImage);
            mediaPlayerApplication.playButtonSound(); // Play the sound when the button is clicked
        });
        ImageButton soundButton = findViewById(R.id.soundButton);
        soundButton.setOnClickListener(v -> {
            isSoundMuted = !isSoundMuted;

            // Mute or unmute sounds based on the isSoundMuted flag
            if (isSoundMuted) {
                mediaPlayerApplication.muteSounds();
            } else {
                mediaPlayerApplication.unmuteSounds();
            }

            // Update the sound icon based on the mute state
            int soundIconResId = isSoundMuted ? R.drawable.volume_off : R.drawable.volume_on;
            soundButton.setImageResource(soundIconResId);
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        mediaPlayerApplication.pauseBackgroundMusic();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mediaPlayerApplication.resumeBackgroundMusic();
    }
}
