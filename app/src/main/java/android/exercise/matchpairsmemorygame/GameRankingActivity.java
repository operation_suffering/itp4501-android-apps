package android.exercise.matchpairsmemorygame;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class GameRankingActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private final List<String> items = new ArrayList<>();
    private ArrayAdapter<String> adapter;
    private MediaPlayerApplication mediaPlayerApplication;

    @SuppressLint("StaticFieldLeak")
    private class DownloadTask extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... value) {
            InputStream inputStream;
            StringBuilder result = new StringBuilder();
            URL url;

            try {
                url = new URL(value[0]);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();

                // Make GET request
                con.setRequestMethod("GET");
                con.connect();

                // Get response string from input stream of connection
                inputStream = con.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = bufferedReader.readLine()) != null)
                    result.append(line);
                inputStream.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return result.toString();
        }


        protected void onPostExecute(String result) {
            items.clear();
            try {
                JSONArray jsonArray = new JSONArray(result);

                // Sort the JSON array by "Moves" field in ascending order
                List<JSONObject> jsonList = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    jsonList.add(jsonArray.getJSONObject(i));
                }
                jsonList.sort((jsonObject1, jsonObject2) -> {
                    int moves1 = jsonObject1.optInt("Moves");
                    int moves2 = jsonObject2.optInt("Moves");
                    return Integer.compare(moves1, moves2);
                });

                // Add sorted items to the list
                for (int i = 0; i < jsonList.size(); i++) {
                    JSONObject jsonObject = jsonList.get(i);
                    String name = jsonObject.getString("Name");
                    int moves = jsonObject.getInt("Moves");
                    String listItem = "Rank: " + (i + 1) + ", Name: " + name + ", Moves: " + moves;
                    items.add(listItem);
                }

                adapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_ranking_activity);
        ListView listViewCode = findViewById(R.id.lv_selection);
        listViewCode.setOnItemClickListener(this);

        mediaPlayerApplication = (MediaPlayerApplication) getApplicationContext();

        adapter = new ArrayAdapter<>(this, R.layout.listview, items);
        listViewCode.setAdapter(adapter);

        // Execute the download task directly with the specified URL
        DownloadTask task = new DownloadTask();
        task.execute("https://ranking-mobileasignment-wlicpnigvf.cn-hongkong.fcapp.run");
    }

    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        // Handle item click if needed
    }

    @Override
    protected void onPause() {
        super.onPause();
        mediaPlayerApplication.pauseBackgroundMusic();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mediaPlayerApplication.resumeBackgroundMusic();
    }
}