package android.exercise.matchpairsmemorygame;

import android.app.Application;
import android.media.MediaPlayer;

import java.util.Random;

public class MediaPlayerApplication extends Application {

    private MediaPlayer backgroundMediaPlayer;
    private MediaPlayer buttonMediaPlayer;
    private boolean isSoundMuted = false;

    @Override
    public void onCreate() {
        super.onCreate();
        setBackgroundMediaPlayer();
    }

    public void setBackgroundMediaPlayer() {
        backgroundMediaPlayer = MediaPlayer.create(this, R.raw.original);
        backgroundMediaPlayer.setLooping(true);
        backgroundMediaPlayer.start();
    }

    public void playButtonSound() {
        int[] catSounds = {
                R.raw.meow1,
                R.raw.meow2,
                R.raw.meow3,
                R.raw.meow4
        };
        int randomIndex = new Random().nextInt(catSounds.length);

        // Check if the sounds are muted
        float volume = isSoundMuted ? 0f : 1f;

        buttonMediaPlayer = MediaPlayer.create(getApplicationContext(), catSounds[randomIndex]);
        buttonMediaPlayer.setVolume(volume, volume);
        buttonMediaPlayer.start();
    }

    public void playMatchSound() {
        // Check if the sounds are muted
        float volume = isSoundMuted ? 0f : 1f;

        buttonMediaPlayer = MediaPlayer.create(this, R.raw.kids_yay);
        buttonMediaPlayer.setVolume(volume, volume);
        buttonMediaPlayer.start();
    }

    public void playFinishSound() {
        // Check if the sounds are muted
        float volume = isSoundMuted ? 0f : 1f;

        buttonMediaPlayer = MediaPlayer.create(this, R.raw.wow_2);
        buttonMediaPlayer.setVolume(volume, volume);
        buttonMediaPlayer.start();
    }

    public void stopBackgroundMusic() {
        if (backgroundMediaPlayer != null && backgroundMediaPlayer.isPlaying()) {
            backgroundMediaPlayer.stop();
            backgroundMediaPlayer.release();
            backgroundMediaPlayer = null;
        }
    }

    public void muteSounds() {
        // Mute all sounds
        if (backgroundMediaPlayer != null && backgroundMediaPlayer.isPlaying()) {
            backgroundMediaPlayer.setVolume(0f, 0f);
        }
        if (buttonMediaPlayer != null && buttonMediaPlayer.isPlaying()) {
            buttonMediaPlayer.setVolume(0f, 0f);
        }
        isSoundMuted = true;
    }

    public void unmuteSounds() {
        // Unmute all sounds
        if (backgroundMediaPlayer != null && backgroundMediaPlayer.isPlaying()) {
            backgroundMediaPlayer.setVolume(1f, 1f);
        }
        if (buttonMediaPlayer != null && buttonMediaPlayer.isPlaying()) {
            buttonMediaPlayer.setVolume(1f, 1f);
        }
        isSoundMuted = false;
    }

    public void pauseBackgroundMusic() {
        if (backgroundMediaPlayer != null && backgroundMediaPlayer.isPlaying()) {
            backgroundMediaPlayer.pause();
        }
    }

    public void resumeBackgroundMusic() {
        if (backgroundMediaPlayer != null && !backgroundMediaPlayer.isPlaying()) {
            backgroundMediaPlayer.start();
        }
    }
}
